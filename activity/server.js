const http = require('http');

http.createServer(function(req, res) {
  if (req.url == "/" && req.method == "GET") {
    res.writeHead(200, {"Content-Type": "text/plain"});
    res.end("Welcome to Booking System!");
  }

if (req.url == "/profile" && req.method == "GET") {
    res.writeHead(200, {"Content-Type" : "text/plain"});
    res.end("Welcome to your Profile!");
  }
}).listen(port);
console.log(`Server is running at localhost:4000`);